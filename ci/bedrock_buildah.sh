#!/bin/bash

# VARIABLES
container=$(buildah from fedora-minimal)
release=$(curl -s https://www.minecraft.net/en-us/download/server/bedrock | awk -F / '/linux\/bedrock-server-/ { print $5 }' | cut -d - -f 3 | cut -d . -f 1,2,3,4)
archive=bedrock-server-${release}.zip
download=https://minecraft.azureedge.net/bin-linux/${archive}

# BUILDAH
buildah config --author='@IMetZach' ${container}
buildah config --label description='The vanilla MineCraft Bedrock Server' ${container}
buildah config --label Name='MineCraft Bedrock Server' ${container}
buildah config --label version=${release} ${container}

buildah config --workingdir='/opt/minecraft' ${container}

buildah run